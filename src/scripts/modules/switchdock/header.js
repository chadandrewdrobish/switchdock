import React from 'react';

import {
    Link, useLocation
} from 'react-router-dom';

// const data object for navigation
const nav = [
    {
        name: 'Benefits',
        pathname: '/'
    }, {
        name: 'Safety',
        pathname: '/safety'
    }, {
        name: 'Policies',
        pathname: '/policies'
    }
];

/**
 * Header
 * Handle's navigation using react-router's Link
 * 
 */

const Header = () => {

    const { pathname } = useLocation();

    return <header className="sd__header">
        <div className="sd__logo-wrapper">
            <img className="sd__logo" src="img/logo_white.png" alt="Switch Logo" height="25px" />
        </div>
        <nav className="sd__nav">
            <ul>
                { nav.map((obj, index) => {
                    // check to see which pathname matches current location
                    const className = pathname === obj.pathname ? 'active' : '';
                    return <li key={`nav-item-${index}`} className="sd__nav-item">
                        <Link className={className} to={obj.pathname} >{ obj.name }</Link>
                    </li>
                }) }
            </ul>
        </nav>
    </header>
};

export default Header;