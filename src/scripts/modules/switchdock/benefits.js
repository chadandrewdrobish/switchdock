import React, { useState, useEffect } from 'react';
import Search from './search'


const SwitchDockBenefits = props => {

    const [status, setStatus] = useState({
        hasLoaded: false,
        data : undefined,
        benefits: [],
        text: ''
    });

    useEffect(() => {
        if(!status.hasLoaded) {
            fetch('./benefits.json')
                .then(response => { return response.json() })
                .then(data => {
                    setStatus({ hasLoaded: true, data, benefits : data.benefits });
                }, error => {
                    setStatus({ hasLoaded: true, data });
                });  
        } 
    });

    const {
        hasLoaded,
        benefits,
        data,
        text
    } = status;

    if(hasLoaded) {
        if (!data) return <Error title="Undefined data" description="The file <a href='./benefits.json'>./benefits.json</a> may not be formatte properly." />;
        if (!data.benefits) return <Error title="Benefits not found" description="The file <a target='_blank' href='./benefits.json'>./benefits.json</a> may not be formatted properly." />;
    
        // benefits = status.data.benefits.map(benefit => (benefit));

        const handleSearchUpdate = text => {

            console.log(`handleSearchUpdate:${text}`);

            if(text === '') {
                setStatus({
                    hasLoaded,
                    benefits: data.benefits,
                    data,
                    text
                });
            } else {
                
                setStatus({
                    hasLoaded,
                    benefits: data.benefits.filter(benefit => {
                        return (benefit.title.toLowerCase().indexOf(text.toLowerCase()) > -1 || benefit.description.toLowerCase().indexOf(text.toLowerCase()) > -1);
                    }),
                    data,
                    text
                });
                
            }
        };
        if (benefits.length === 0) {
            return [
                <Search key="search" onUpdate={handleSearchUpdate} />,
                <Error key="error" title="No benefits found." description={`No benefits found with the serach criteria of <strong>${text}</strong>`} />
            ]
        }
        return [
            <Search key="search" onUpdate={handleSearchUpdate} /> ,
            <ul key="list" className="benefit__list">
                {benefits.map((benefit, index) => (<Benefit key={`benefit-${index}`} {...benefit} />))}
            </ul>
        ];

    } else {
        return <div/>
    }


}

const Error = props => (
    <div className="sd__error">
        <h4 className="sd__error-header">{props.title}</h4>
        <div className="sd__error-message">
            <i className="sd__error-icon fas fa-exclamation-circle"></i>
            <p className="sd__error-message-body" dangerouslySetInnerHTML={{__html:props.description}}></p>
        </div>
        
    </div>
);

const Benefit = props => (
    <li className="benefit__list-item">
        <h4 className="benefit__list-item-header">{ props.title }</h4>
        <p className="benefit__list-item-body">{ props.description }</p>
    </li>
);

export default SwitchDockBenefits