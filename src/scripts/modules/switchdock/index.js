import React, { Fragment } from 'react';
import { render } from 'react-dom';

import Header from './header';
import Benefits from './benefits';
import Safety from './safety';
import Policies from './policies';

import {
    BrowserRouter as Router,
    Switch,
    Route
} from 'react-router-dom';

/**
 * Main js for app
 * Includes header, and routing using react-router
 */

const basename = process.env.NODE_ENV === 'production' ? '/Switch/SwitchDock/' : '' ;
console.log(process.env)

export default class SwitchDock {
    constructor(el) {
        render(<Router basename={basename}>
            <Header key="switch-header" />
            <Switch>
                <Fragment>
                    <section className="sd__section">
                        <Route path={["", "./"]} exact computedmatch>
                            <Benefits />
                        </Route>
                        <Route path="/safety">
                            <Safety />
                        </Route>
                        <Route path="/policies">
                            <Policies />
                        </Route>
                    </section>
                </Fragment>
            </Switch>
        </Router>, el)
    }
}