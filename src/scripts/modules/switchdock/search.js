import { debounce } from 'lodash';
import React, { useState } from 'react';
import PropTypes from 'prop-types';

/**
 * Search Input
 *  
 */

const Search = props => {

    const [text, setText] = useState('');

    // debounce keystrokes
    const update = debounce((value) => {
        props.onUpdate(value);
    }, 400);
    
    // handle onChange event from input
    const handleChange = e => {
        const { value }  = e.target;
        // set component text
        setText(value);
        // send update callback
        update(value);
    }
    
    // render
    return <div className="sd__search-wrapper">
        <div className="sd__search-input">
            <input onChange={handleChange} placeholder="Search…" value={text} />
            <i className="fas fa-search sd__search-icon"></i>
        </div>
    </div>
}

Search.propTypes = {
    onUpdate: PropTypes.func
}

export default Search;