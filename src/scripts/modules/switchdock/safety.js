import React from 'react';
/**
 * Placeholder Safety page 
 */
const safety = () => (<div className="sd__safety">
    <h1>Safety</h1>
    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid, excepturi sapiente numquam asperiores tenetur quidem ea iure fugit similique maxime ipsam beatae iste, incidunt earum consequatur fuga sit explicabo nam.</p>
    <h3>Veritatis rem odio, molestiae explicabo.</h3>
    <ul>
        <li>Deserunt, placeat officia incidunt ea ipsum sit animi dolores aut suscipit dignissimos corporis quas cumque vel debitis quos laudantium natus illo ut!</li>
        <li>Voluptatum quaerat illum recusandae minima eligendi ipsa eius vitae facilis quasi eaque iste cupiditate, quis veniam, numquam dolores aliquid. Officiis, a quaerat.</li>
        <li>Ea quidem maiores totam eveniet expedita assumenda temporibus nam saepe natus debitis, aperiam id soluta dolores unde mollitia exercitationem magnam eos quasi.</li>
        <li>Architecto dignissimos omnis provident at, magnam beatae expedita nostrum tempore voluptatum dicta et optio perspiciatis sed in explicabo aliquid officiis inventore. Ipsam.</li>
        <li>Voluptatem quidem possimus, sint nam alias repellat. Corrupti adipisci nobis, soluta molestiae dolores unde similique porro necessitatibus sapiente ipsam minus voluptatibus! Voluptate?</li>
        <li>Odit sunt eveniet facere recusandae distinctio laboriosam quam impedit exercitationem perspiciatis repellat.</li>
    </ul>
</div>);
export default safety;