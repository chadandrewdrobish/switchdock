import React from 'react';
/**
 * Placeholder Policies page
 */
const policies = () => (<div className="sd__policies">
    <h1>Policies</h1>
    <p>Ab ratione sunt molestiae esse dolor totam eligendi repellat ex, quidem reiciendis exercitationem, facere quo? Dignissimos libero recusandae nemo ipsam quas repellendus.</p>
    <p>Eligendi cupiditate ab, non provident atque ut ea tenetur rerum quaerat dicta hic obcaecati. Quaerat, impedit consequuntur voluptatibus itaque tempore ut odit.</p>
    <p>Sit, aperiam. Architecto officia quod cumque quos rerum accusantium eligendi ex voluptate labore doloribus repellat exercitationem harum necessitatibus, quae, veritatis dolores eos.</p>
    <p> Unde deserunt eligendi, qui voluptates culpa, minus, tenetur distinctio consequatur odio enim exercitationem. Veritatis est doloremque odit, officia quas rem eos ducimus!</p>

</div>);
export default policies;