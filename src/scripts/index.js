import SwitchDock from './modules/switchdock/index';

// init application

const switchEl = document.querySelector('[data-module=switchdock]');

if (switchEl) {
    const sd = new SwitchDock(switchEl);
}