### Switch Dock

[See working example here](https://clients.chadandrewdrobish.com/Switch/SwitchDock/)


## The Challenge
Produce a single page web application which includes all of the data in the json file and renders it according to the mocks provided.  You may use whatever templating solutions or dynamic CSS formats you feel comfortable with.

## Objectives
1. Demonstrate ability to generate clean mark-up from a json response as one might do when getting data dynamically from a REST API.

2. Match design mocks as closely as possible

3. Show command of website responsiveness

### Optional Bonus Points

1. ~~Introduce animations or transitions~~
2. Introduce interactivity (text search, acknowledgement of benefits, drag/drop of tiles, or whatever you like) **Added search box**
3. ~~Re-theme as light text on dark background while maintaining branded look & feel~~

## Librairies Used

For compiling : [Blendid](https://github.com/vigetlabs/blendid)

Javascript
**React**
**React-Router**
**Lodash**

Extras
**Font Awesome**
